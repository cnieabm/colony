import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def plot_field(nut_field_path):
    nut_field = [[float(i) for i in line] for line in csv.reader(open(nut_field_path), delimiter=",")]
    siz = len(nut_field)
    print(siz, len(nut_field[0]))
    xgrid = [[i/10 for i in range(siz)]]*siz
    ygrid = [[i/10]*siz for i in range(siz)]

    # print(xgrid)
    # print(ygrid)
    # print(nut_field)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_wireframe(xgrid, ygrid, nut_field)

field_path = "NumericalField00.csv"
plot_field(field_path)
plt.title("Numerical Field t=0.0s")
field_path = "NumericalField05.csv"
plot_field(field_path)
plt.title("Numerical Field t=0.5s")

plt.show()
