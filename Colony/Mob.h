//
//  Mob.h
//  compevol
//
//  Created by Michael Michaelides on 15/01/2014.
//  Copyright (c) 2014 Comp Evol. All rights reserved.
//

#ifndef compevol_Mob_h
#define compevol_Mob_h
#include "Entity.h"

//using namespace std;

class Mob: public Entity{
	
	//list <Entity> Pred=NULL; //replace with types (enum?)
	//list <Entity> Prey=NULL;
	
protected:
//	AdjacentSites nearest;
//	moveList pattern;
//	typedef vector<PossMove> possList;
	double combatValue, RepThreshold;
	int generation;
	double vision;
	double AvgOffspring;
	double massChange, statfoodit;
	double foodTransferRate;
    double mass;
    double radius;
    double stepAngle; //the MFP (total dist travelled at an angle)
    double distSameAngle; //distance travelled at current angle
    
	//food cap variables
//	double foodCap;
	double digestit;
	
	//mutating variables for attaction to same, prey and pred
//	double mutPrey, mutPred, mutSame;

	Mob(){ //used for inheritance only
		
	}
//	Mob(float a, float b, Settings* simSettings){
//		setting = simSettings;
//		pos<<a,
//            b;
//        posnew = pos;
//	}
public:
	double GetRepThreshold()override{return RepThreshold;}
	double GetCombatValue()override{ return combatValue; }
	bool GetRepoduce()override{ return reproduce; }
	virtual void PrepareMove(Entity * nutrient_field){}
	double GetAvgOffspring()override{return AvgOffspring;}
	double GetTransferRate()override {return foodTransferRate;}
//	double GetFoodCap()override {return foodCap;}
	double GetDigestIt()override {return digestit;}
    // getters for model parameters
    virtual double GetParameterA() {return 1.0;}
    virtual double GetParameterC() {return 1.0;}
    virtual double GetParameterE() {return 1.0;}
    
	void update(vector <Entity*>& entities) override{
		if (true){
			bool posClear = true;
			
	//		for (const auto &entitya : entities){
	//			if (entitya->getx() == Mod(xnew, systemX) && entitya->gety() == Mod(ynew, systemY)){
	//				posClear = false;
	//			}
	//		}
            
            
			if (posClear){
                pos = posnew;
//				if (mass > 0){
					mass+=massChange;
//				}
//				else{
//					Inactivate();
//				}
			}
            if (mass<=0){
                active=false;
                mass = 0.0005;
            }
            else{
                active=true;
            }
		}
	}
	
	void haveReproduced(int num) override{
		mass=0.0005;
		reproduce=false;
	}
	
//	void PopFreeSite (int& x, int& y) override{
//		int option = Mod(abs(int(generator())),nearest.GetNumFreeSites());
//		if(!nearest.upOccupied && option == 0){
//			nearest.upOccupied = true;
//			y++;
//		}
//		else{
//			option--;
//			if(!nearest.downOccupied && option == 0){
//				nearest.downOccupied = true;
//				y--;
//			}
//			else{
//				option--;
//				if(!nearest.rightOccupied && option == 0){
//					nearest.rightOccupied = true;
//					x++;
//				}
//				else{
//					option--;
//					if(!nearest.leftOccupied && option == 0){
//						nearest.leftOccupied = true;
//						x--;
//					}
//				}
//			}
//		}
//	}
    
	int GetNumOffspring(){
//		int num = int( mutation(AvgOffspring,1) + 0.5);
//		if(num > nearest.GetNumFreeSites()){
//			num = nearest.GetNumFreeSites();
//		}
//		else if(num<1)
//			num=1;
//		return num;
        return AvgOffspring;
    }
	
	void Eat(Entity* victim){
        mass += victim->GetMass();
        victim->RemoveFood(victim->GetMass());
	}
	
	double GetMass() override{
		return mass;
	}
	
//	double GetMutPred() override{
//		return mutPred;
//	}
//	
//	double GetMutPrey() override{
//		return mutPrey;
//	}
//	
//	double GetMutSame() override{
//		return mutSame;
//	}
	
	int GetGeneration() override{return generation;}
	double GetVision() override{return vision;}
	double GetStatfoodit() override{return statfoodit;}
	double GetMovefoodit() override{return massChange;}
    double GetStepAngle()override{return stepAngle;}
    double GetDistSameAngle()override{return distSameAngle;}
//	moveList GetMoveList() override{return pattern;}
    
    // getter for mass change
    virtual double GetMassChange() {return massChange;}
	
	virtual ~Mob(){};
};

#endif
