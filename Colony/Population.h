#ifndef POPULATION_H
#define POPULATION_H

class Population{
public:
	int food, bug, predator;
	void Reset(){
		food = 0;
		bug = 0;
		predator = 0;
	}
	void AddEntity(int type){
		switch (type)
		{
		case 1:
			food++;
			break;
		case 0:
			bug++;
			break;
		case 2:
			predator++;
			break;
		}
	}
};

#endif