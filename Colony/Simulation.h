//
//  Simulation.h
//  compevol
//
//  Created by Michael Michaelides on 15/01/2014.
//  Copyright (c) 2014 Comp Evol. All rights reserved.
//

#ifndef compevol_Simulation_h
#define compevol_Simulation_h
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <random>

#include "Colour.h"
#include "Screen.h"
#include "Entity.h"
#include "Mob.h"
#include "Bacterium.h"
#include "NutrientField.h"
//#include "Food.h"
//#include "Predator.h"
#include "Population.h"
//#include "GeneBins.h"
//#include "2DBins.h"
#include "Settings.h"

using namespace std;

class Simulation{
	
public:	
	vector <Entity*> entities;
	
	enum FileType {Text, Video};
	
	bool anyAlive;
	Settings* setting;
	
	Screen Display;
	bool IsDead(const Entity& entitya){
		return !(entitya.alive);
	}

	Simulation(Settings* set){
		setting = set;
		Display = Screen(setting->systemX,setting->systemY);
	}
	void Run(){
		string outpath ="";
		outpath += setting->path;
        double time = 0.;
		//ofstream outTextFile(AppendToString(GetFileName(),GetFileExtention(FileType::Text)).c_str());
		//binned
//		ofstream outBugRepGenes(outpath+"BugRep.txt");
//		ofstream outPredRepGenes(outpath+"PredRep.txt");
//		ofstream outBugOffGenes(outpath+"BugOffspring.txt");
//		ofstream outPredOffGenes(outpath+"PredOffspring.txt");
//		
//		ofstream outBugStatA(outpath+"BugStatPred.txt");
//		ofstream outBugStatB(outpath+"BugStatPrey.txt");
//		ofstream outBugStatC(outpath+"BugStatSame.txt");
//		ofstream outPredStatA(outpath+"PredStatPred.txt");
//		ofstream outPredStatB(outpath+"PredStatPrey.txt");
//		ofstream outPredStatC(outpath+"PredStatSame.txt");
//		
//		ofstream outPopfile(outpath+"Populations.txt");
//		ofstream outGenfile(outpath+"Generation.txt");
//
//		ofstream outpredRepOffspring(outpath+"2DBins/predRepOffspring.txt");
//		ofstream outpredRepFood(outpath+"2DBins/predRepFood.txt");
//		ofstream outpredRepBug(outpath+"2DBins/predRepBug.txt");
//		ofstream outpredRepPred(outpath+"2DBins/predRepPred.txt");
//
//		ofstream outpredOffspringFood(outpath+"2DBins/predOffspringFood.txt");
//		ofstream outpredOffspringBug(outpath+"2DBins/predOffspringBug.txt");
//		ofstream outpredOffspringPred(outpath+"2DBins/predOffspringPred.txt");
//
//		ofstream outpredFoodBug(outpath+"2DBins/predFoodBug.txt");
//		ofstream outpredFoodPred(outpath+"2DBins/predFoodPred.txt");
//		ofstream outpredBugPred(outpath+"2DBins/predBugPred.txt");
//
//		ofstream outbugRepOffspring(outpath+"2DBins/bugRepOffspring.txt");
//		ofstream outbugRepFood(outpath+"2DBins/bugRepFood.txt");
//		ofstream outbugRepBug(outpath+"2DBins/bugRepBug.txt");
//		ofstream outbugRepPred(outpath+"2DBins/bugRepPred.txt");
//
//		ofstream outbugOffspringFood(outpath+"2DBins/bugOffspringFood.txt");
//		ofstream outbugOffspringBug(outpath+"2DBins/bugOffspringBug.txt");
//		ofstream outbugOffspringPred(outpath+"2DBins/bugOffspringPred.txt");
//
//		ofstream outbugFoodBug(outpath+"2DBins/bugFoodBug.txt");
//		ofstream outbugFoodPred(outpath+"2DBins/bugFoodPred.txt");
//		ofstream outbugBugPred(outpath+"2DBins/bugBugPred.txt");
//
//		//not binned
//		ofstream outBugSpeciesfile(outpath+"BugSpeciesDevelopment.txt");
//		ofstream outPredSpeciesfile(outpath+"PredSpeciesDevelopment.txt");
		
		//Set up the simulation
		SetUpSim();
        entities[0]->test_initial();
        entities[0]->print_field(outpath + "output_test/NumericalField00.csv");

        //SaveEntities(outTextFile);
		Display.disp();
		int numOffspring;
		int i;
//        entities[0]->update(entities);
		do{
            //Entities interact
            entities[0]->update_entity_list(entities);  // updating entity list for nutrient field
            for (const auto &entitya : entities){
                //				if(setting->predatorExtinction == iteration && entitya->type == Entity::EntityType::Predator){
                //					entitya->Kill();
                //				}
                if (entitya->IsActive()){
                    entitya->PrepareMove(entities[0]);
                }
            }

			anyAlive = true;
			vector<Entity*> entitiesCopy = entities;
			entities.clear();
			population.Reset();
			for (const auto &entitya : entitiesCopy){
				if (entitya->alive){
					if(entitya->type != Entity::EntityType::NutrientField){
						anyAlive = true;
					}
					if (entitya->GetRepoduce()){
						numOffspring = entitya->GetNumOffspring();
						entitya->haveReproduced(numOffspring);
						if (entitya->type == Entity::EntityType::Bacterium){
							for(i = 0; i < numOffspring; i++){
								entities.push_back(new Bacterium(entitya, setting));
								population.bug++;
							}
						}
//						else if (entitya->type == Entity::EntityType::Predator){
//							for(i = 0; i < numOffspring; i++){
//								entities.push_back(new Predator(entitya, setting));
//								population.predator++;
//							}
//						}
					}
					entities.push_back(entitya);
					population.AddEntity(entitya->type);
				}
				else{
					delete entitya; //free up memory
				}
                
			}
            //Update Environment
            //UpdateFood();
            entities[0]->update(entities);
            
            //Update agents
			entitiesCopy.clear();
			for (const auto &entitya : entities){
                if (entitya->getType()==Entity::EntityType::Bacterium){
                    entitya->update(entities);
                }
			}
            
			cout << iteration <<"\t"<<population.bug<<"\t"<<population.predator<<endl;
			iteration++;
            time += entities[0]->GetTimestep();
            if (time >= 0.5){
                entities[0]->print_field(outpath + "output_test/NumericalField05.csv");
                anyAlive = false;
            }
			//Display
			DisplayEntities();
//			PrintEntities();

			
			//Save Data
//			SaveEntityGenes(outBugRepGenes, outPredRepGenes, outBugOffGenes, outPredOffGenes);
//			SaveEntityStats(outBugStatA,outBugStatB, outBugStatC, outPredStatA, outPredStatB, outPredStatC);
//			SavePopulation(outPopfile);
//			SaveGenerations(outGenfile);
//			SaveSpecies( outpredRepOffspring, outpredRepFood, outpredRepBug, outpredRepPred
//		, outpredOffspringFood, outpredOffspringBug, outpredOffspringPred
//		, outpredFoodBug, outpredFoodPred, outpredBugPred
//		, outbugRepOffspring, outbugRepFood, outbugRepBug, outbugRepPred
//		, outbugOffspringFood, outbugOffspringBug, outbugOffspringPred
//		,outbugFoodBug,outbugFoodPred, outbugBugPred);
			
			//endlines for each iteration (gnuplot 3d)
//			outBugRepGenes<<endl;
//			outPredRepGenes<<endl;
//			outPopfile<<endl;
//			outGenfile<<endl;
//			outBugOffGenes<<endl;
//			outPredOffGenes<<endl;
//			outBugSpeciesfile<<endl;
//			outPredSpeciesfile<<endl;
//			
//			outBugStatA<<endl;
//			outBugStatB<<endl;
//			outBugStatC<<endl;
//			outPredStatA<<endl;
//			outPredStatB<<endl;
//			outPredStatC<<endl;

//			outpredRepOffspring<<endl;
//			outpredRepFood<<endl;
//			outpredRepBug<<endl;
//			outpredRepPred<<endl;
//			
//			outpredOffspringFood<<endl;
//			outpredOffspringBug<<endl;
//			outpredOffspringPred<<endl;
//			
//			outpredFoodBug<<endl;
//			outpredFoodPred<<endl;
//			outpredBugPred<<endl;
//			
//			outbugRepOffspring<<endl;
//			outbugRepFood<<endl;
//			outbugRepBug<<endl;
//			outbugRepPred<<endl;
//			
//			outbugOffspringFood<<endl;
//			outbugOffspringBug<<endl;
//			outbugOffspringPred<<endl;
//			
//			outbugFoodBug<<endl;
//			outbugFoodPred<<endl;
//			outbugBugPred<<endl;

		} while (anyAlive);
		
//		outBugRepGenes.close();
//		outPredRepGenes.close();
		//loop until End Conditon

		//Finish Sim Run
		Display.end();
	}
//	void CreatePredator(int xPos, int yPos){
//		bool posClear = true;
//		for (const auto &entitya : entities){
//			if (entitya->getx() == xPos && entitya->gety() == yPos){
//				posClear = false;
//			}
//		}
//		
//		//create food if position is clear
//		if (posClear){
//			entities.push_back(new Predator(xPos, yPos,setting));
//		}
//	}
    
	bool CreateBacterium(float xPos, float yPos){
		bool posClear = true;
//		for (const auto &entitya : entities){
//			if (entitya->getx() == xPos && entitya->gety() == yPos){
//				posClear = false;
//			}
//		}
//		
		//create bug if position is clear
		if (posClear){
			entities.push_back(new Bacterium(5., 5.,setting));
		}
		return posClear;
	}
	void CreateNutrientField(int xPos, int yPos){
		//create nutrient field
//        const char* vars[] = {"2D", "/Users/michalis/Documents/UCL/ABM/Colony/in.xda", "1", "0.01", "1", "1.0", "2.0"};
        
        entities.push_back(new NutrientField(setting)); //speed deltat maxt alpha delta
	}
	
private:
	//Define 2D array of letters
	int iteration;
	char* screen;

	Population population;
//	GeneBins bugRepBins;
//	GeneBins predRepBins;
//	GeneBins bugOffBins;
//	GeneBins predOffBins;
//	GeneBins bugStatABins;
//	GeneBins bugStatBBins;
//	GeneBins bugStatCBins;
//	GeneBins predStatABins;
//	GeneBins predStatBBins;
//	GeneBins predStatCBins;
//	GeneBins genBins;
//
//	TwoDBins predRepOffspring;
//	TwoDBins predRepFood;
//	TwoDBins predRepBug;
//	TwoDBins predRepPred;
//
//	TwoDBins predOffspringFood;
//	TwoDBins predOffspringBug;
//	TwoDBins predOffspringPred;
//
//	TwoDBins predFoodBug;
//	TwoDBins predFoodPred;
//	TwoDBins predBugPred;
//	
//	TwoDBins bugRepOffspring;
//	TwoDBins bugRepFood;
//	TwoDBins bugRepBug;
//	TwoDBins bugRepPred;
//
//	TwoDBins bugOffspringFood;
//	TwoDBins bugOffspringBug;
//	TwoDBins bugOffspringPred;
//
//	TwoDBins bugFoodBug;
//	TwoDBins bugFoodPred;
//	TwoDBins bugBugPred;


	char& operator()(int i, int j) { return screen[setting->systemX*i + j]; }
	mt19937_64 generator; //[README] 64bit machines only! If you have 32bit change mt19937_64 to mt19937
#pragma region Utility Functions
	int Mod(int A, int B){
		if(A<0){
			return Mod(A+B,B);
		}
		else{
			return A%B;
		}
	}
#pragma endregion
#pragma region set up simulation
	void SetUpSim(){
		random_device seed;
		generator.seed(seed()); // use seed as generators seed

		screen = new char[setting->systemX*setting->systemY];
		//Distribute entities
		DistributeNutrients();
//		DistributeBacteria();
//        entities.push_back(new Bacterium((int)(setting->systemX/2), (int)(setting->systemY/2), setting));

//		if(setting->predators){
//			DistributePredators();
//		}

//		bugStatABins = GeneBins(0.02);
//		bugStatBBins = GeneBins(0.02);
//		bugStatCBins = GeneBins(0.02);
//		predStatABins = GeneBins(0.02);
//		predStatBBins = GeneBins(0.02);
//		predStatCBins = GeneBins(0.02);
//		genBins = GeneBins(1.0);
//
//		predRepOffspring = TwoDBins(150,0,0.5,0.1);
//		predRepFood = TwoDBins(150,0,0.5,0.02);
//		predRepBug = TwoDBins(150,0,0.5,0.02);
//		predRepPred = TwoDBins(150,0,0.5,0.02);
//		
//		predOffspringFood = TwoDBins(0,0,0.1,0.02);
//		predOffspringBug = TwoDBins(0,0,0.1,0.02);
//		predOffspringPred = TwoDBins(0,0,0.1,0.02);
//		
//		predFoodBug = TwoDBins(0,0,0.02,0.02);
//		predFoodPred = TwoDBins(0,0,0.02,0.02);;
//		predBugPred = TwoDBins(0,0,0.02,0.02);;
//		
//		bugRepOffspring = TwoDBins(80,0,0.5,0.1);
//		bugRepFood = TwoDBins(80,0,0.5,0.02);
//		bugRepBug = TwoDBins(80,0,0.5,0.02);
//		bugRepPred = TwoDBins(80,0,0.5,0.02);
//		
//		bugOffspringFood = TwoDBins(0,0,0.1,0.02);
//		bugOffspringBug = TwoDBins(0,0,0.1,0.02);
//		bugOffspringPred = TwoDBins(0,0,0.1,0.02);
//		
//		bugFoodBug = TwoDBins(0,0,0.02,0.02);
//		bugFoodPred = TwoDBins(0,0,0.02,0.02);;
//		bugBugPred = TwoDBins(0,0,0.02,0.02);;
//		//start bins
//		bugRepBins.StartBin(80);
//		predRepBins.StartBin(150);
//		genBins.StartBin(1);
//		bugOffBins.StartBin(2);
//		predOffBins.StartBin(2);
//		bugStatABins.StartBin(0);
//		bugStatBBins.StartBin(0);
//		bugStatCBins.StartBin(0);
//		predStatABins.StartBin(0);
//		predStatBBins.StartBin(0);
//		predStatCBins.StartBin(0);
		
		//		PrintEntities();
		iteration = 0;
	}
	void DistributeNutrients(){
//		int NumSites;
		switch(setting->foodDistribution){
//            case Settings::FoodDistType::Single:
//				CreateFood();
//				break;
//			case Settings::FoodDistType::Few:
//				NumSites = Mod(abs(int(generator())) , int(0.001*setting->systemX * setting->systemY) + 1);
//				for(int i = 0; i < NumSites; i++){
//					CreateFood();
//				}
//				break;
//			case Settings::FoodDistType::Many:
//				NumSites = Mod(abs(int(generator()))  , int(0.2 * setting->systemX * setting->systemY)) + int(0.01*setting->systemX * setting->systemY) + 1;
//				for(int i = 0; i < NumSites; i++){
//					CreateFood();
//				}
//				break;
            case Settings::FoodDistType::NutrientField:
                CreateNutrientField();
                break;
			default:
				break;
		}
	}
	void DistributeBacteria(){
//		int NumSites = Mod(abs(int(generator())), int(0.05*setting->systemX * setting->systemY) + 1);
        int NumSites = 1;
		for (int i = 0; i < NumSites; i++){
            CreateBacterium();
		}
	}
//	void DistributePredators(){
//		int NumSites = Mod(abs(int(generator())), int(0.005*setting->systemX * setting->systemY) + 1);
//		for (int i = 0; i < NumSites; i++){
//			CreatePredator();
//		}
//	}
    
	void CreateBacterium(){
		bool posClear = true;
		float xPos, yPos;
//		do{
			// pick a random square
			posClear = true;
//			xPos = Mod(abs(int(generator())), setting->systemX);
//			yPos = Mod(abs(int(generator())), setting->systemY);
            xPos = 5.;
            yPos = 5.;
            cout<<xPos<<" "<<yPos<<endl;
			CreateBacterium(xPos, yPos);
//		} while (!posClear);
	}
    
//	void CreatePredator(){
//		bool posClear = true;
//		int xPos, yPos;
//		do{
//			// pick a random square
//			posClear = true;
//			xPos = Mod(abs(int(generator())), setting->systemX);
//			yPos = Mod(abs(int(generator())), setting->systemY);
//			
//			CreatePredator(xPos, yPos);
//		} while (!posClear);
//	}
    
	void CreateNutrientField(){
		bool posClear = true;
		int xPos, yPos;//, c=0;
//		do{
//			// pick a random square
//			posClear = true;
			xPos = Mod(abs(int(generator()))  , setting->systemX);
			yPos = Mod(abs(int(generator()))  , setting->systemY);
//		} while ((!posClear));//&&c<11);
		if (posClear) CreateNutrientField(xPos, yPos);
	}
    
//	void CreateFoodLeft(){
//		bool posClear = true;
//		int xPos, yPos;//, c=0;
//		do{
//			// pick a random square
//			posClear = true;
//			xPos = Mod(abs(int(generator()))  , 0.5*setting->systemX);
//			yPos = Mod(abs(int(generator()))  , setting->systemY);
//		} while ((!posClear));//&&c<11);
//		if (posClear) CreateFood(xPos, yPos);
//	}
    
//	void CreateFoodRight(){
//		bool posClear = true;
//		int xPos, yPos;//, c=0;
//		do{
//			// pick a random square
//			posClear = true;
//			xPos = Mod(abs(int(generator()))  , 0.5*setting->systemX) +  0.5*setting->systemX;
//			yPos = Mod(abs(int(generator()))  , setting->systemY);
//		} while ((!posClear));//&&c<11);
//		if (posClear) CreateFood(xPos, yPos);
//	}
#pragma endregion set up simulation
#pragma region simulation iteration
//	void SaveEntities(ofstream& file){
//		SaveEntityPosition(file);
//	}
//	void SaveGenerations(ofstream& file){
//		genBins.clear();
//		for (const auto &entitya : entities){
//			genBins.AddValue(entitya->GetGeneration());
//		}
//		for (int i = 0; i < genBins.bin.size(); i++){
//			file << iteration << "\t" << genBins.bin[i].first << "\t" << genBins.bin[i].second << endl;
//		}
//	}
//	void SaveEntityGenes(ofstream& outBugRepGenes,ofstream& outPredRepGenes,ofstream& outBugOffGenes,ofstream& outPredOffGenes){
//		bugRepBins.clear();
//		bugOffBins.clear();
//		predRepBins.clear();
//		predOffBins.clear();
//		for (const auto &entitya : entities){
//			if (entitya->getType() == Entity::EntityType::Bug){
//				bugRepBins.AddValue(entitya->GetRepThreshold());
//				bugOffBins.AddValue(entitya->GetAvgOffspring());
//			}
//			else if (entitya->getType() == Entity::EntityType::Predator){
//				predRepBins.AddValue(entitya->GetRepThreshold());
//				predOffBins.AddValue(entitya->GetAvgOffspring());
//			}
//		}
//		SaveBins(outBugRepGenes,bugRepBins,population.bug);
//		SaveBins(outPredRepGenes,predRepBins,population.predator);
//		SaveBins(outBugOffGenes,bugOffBins,population.bug);
//		SaveBins(outPredOffGenes,predOffBins,population.predator);
//	}
	
//	void SaveEntityStats(ofstream& outBugStatA,ofstream& outBugStatB,ofstream& outBugStatC,ofstream& outPredStatA, ofstream& outPredStatB, ofstream& outPredStatC){
//		bugStatABins.clear();
//		bugStatBBins.clear();
//		bugStatCBins.clear();
//		predStatABins.clear();
//		predStatBBins.clear();
//		predStatCBins.clear();
//		
//		for (const auto &entitya : entities){
//			if (entitya->getType() == Entity::EntityType::Bug){
//				bugStatABins.AddValue(entitya->GetMutPred());
//				bugStatBBins.AddValue(entitya->GetMutPrey());
//				bugStatCBins.AddValue(entitya->GetMutSame());
//			}
//			else if (entitya->getType() == Entity::EntityType::Predator){
//				predStatABins.AddValue(entitya->GetMutPred());
//				predStatBBins.AddValue(entitya->GetMutPrey());
//				predStatCBins.AddValue(entitya->GetMutSame());
//			}
//		}
//		SaveBins(outBugStatA,bugStatABins,population.bug);
//		SaveBins(outBugStatB,bugStatBBins,population.bug);
//		SaveBins(outBugStatC,bugStatCBins,population.bug);
//		SaveBins(outPredStatA,predStatABins,population.predator);
//		SaveBins(outPredStatB,predStatBBins,population.predator);
//		SaveBins(outPredStatC,predStatCBins,population.predator);
//	}
//	void SaveBins(ofstream& file, GeneBins inBins, int pop){
//		for(int i = 0; i < inBins.bin.size(); i++){
//			file << iteration << '\t' << inBins.bin[i].first << '\t' << inBins.bin[i].first<< '\t' << double(inBins.bin[i].second)/double(pop) << endl;
//		}
//	}
	
//	void SaveSpecies(ofstream& outpredRepOffspring,ofstream& outpredRepFood,ofstream& outpredRepBug,ofstream& outpredRepPred
//		,ofstream& outpredOffspringFood,ofstream& outpredOffspringBug,ofstream& outpredOffspringPred
//		,ofstream& outpredFoodBug,ofstream& outpredFoodPred,ofstream& outpredBugPred
//		,ofstream& outbugRepOffspring,ofstream& outbugRepFood,ofstream& outbugRepBug,ofstream& outbugRepPred
//		,ofstream& outbugOffspringFood,ofstream& outbugOffspringBug,ofstream& outbugOffspringPred
//		,ofstream& outbugFoodBug,ofstream& outbugFoodPred,ofstream& outbugBugPred){
//
//		predRepOffspring.Clear();
//		predRepFood.Clear();
//		predRepBug.Clear();
//		predRepPred.Clear();
//
//		predOffspringFood.Clear();
//		predOffspringBug.Clear();
//		predOffspringPred.Clear();
//		
//		predFoodBug.Clear();
//		predFoodPred.Clear();
//		predBugPred.Clear();
//		
//		bugRepOffspring.Clear();
//		bugRepFood.Clear();
//		bugRepBug.Clear();
//		bugRepPred.Clear();
//		
//		bugOffspringFood.Clear();
//		bugOffspringBug.Clear();
//		bugOffspringPred.Clear();
//		
//		bugFoodBug.Clear();
//		bugFoodPred.Clear();
//		bugBugPred.Clear();
//
//		for (const auto &entitya : entities){
//			if (entitya->type == Entity::EntityType::Bug){
//				bugRepOffspring.Add(entitya->GetRepThreshold(),entitya->GetAvgOffspring());
//				bugRepFood.Add(entitya->GetRepThreshold(),entitya->GetMutPrey());
//				bugRepBug.Add(entitya->GetRepThreshold(),entitya->GetMutSame());
//				bugRepPred.Add(entitya->GetRepThreshold(),entitya->GetMutPred());
//				
//				bugOffspringFood.Add(entitya->GetAvgOffspring(),entitya->GetMutPrey());
//				bugOffspringBug.Add(entitya->GetAvgOffspring(),entitya->GetMutSame());
//				bugOffspringPred.Add(entitya->GetAvgOffspring(),entitya->GetMutPred());
//				
//				bugFoodBug.Add(entitya->GetMutPrey(),entitya->GetMutSame());
//				bugFoodPred.Add(entitya->GetMutPrey(),entitya->GetMutPred());
//				bugBugPred.Add(entitya->GetMutSame(),entitya->GetMutPred());
//			}
//			else if (entitya->type == Entity::EntityType::Predator){
//				predRepOffspring.Add(entitya->GetRepThreshold(),entitya->GetAvgOffspring());
//				predRepFood.Add(entitya->GetRepThreshold(),entitya->GetMutPred());
//				predRepBug.Add(entitya->GetRepThreshold(),entitya->GetMutPrey());
//				predRepPred.Add(entitya->GetRepThreshold(),entitya->GetMutSame());
//				
//				predOffspringFood.Add(entitya->GetAvgOffspring(),entitya->GetMutPred());;
//				predOffspringBug.Add(entitya->GetAvgOffspring(),entitya->GetMutPrey());
//				predOffspringPred.Add(entitya->GetAvgOffspring(),entitya->GetMutSame());
//				
//				predFoodBug.Add(entitya->GetMutPred(),entitya->GetMutPrey());
//				predFoodPred.Add(entitya->GetMutPred(),entitya->GetMutSame());
//				predBugPred.Add(entitya->GetMutPrey(),entitya->GetMutSame());
//			}
//		}
//		Save2DBins(outpredRepOffspring,predRepOffspring,population.predator);
//		Save2DBins(outpredRepFood,predRepFood,population.predator);
//		Save2DBins(outpredRepBug,predRepBug,population.predator);
//		Save2DBins(outpredRepPred,predRepPred,population.predator);
//		
//		Save2DBins(outpredOffspringFood,predOffspringFood,population.predator);
//		Save2DBins(outpredOffspringBug,predOffspringBug,population.predator);
//		Save2DBins(outpredOffspringPred,predOffspringPred,population.predator);
//		
//		Save2DBins(outpredFoodBug,predFoodBug,population.predator);
//		Save2DBins(outpredFoodPred,predFoodPred,population.predator);
//		Save2DBins(outpredBugPred,predBugPred,population.predator);
//		
//		Save2DBins(outbugRepOffspring,bugRepOffspring,population.bug);
//		Save2DBins(outbugRepFood,bugRepFood,population.bug);
//		Save2DBins(outbugRepBug,bugRepBug,population.bug);
//		Save2DBins(outbugRepPred,bugRepPred,population.bug);
//		
//		Save2DBins(outbugOffspringFood,bugOffspringFood,population.bug);
//		Save2DBins(outbugOffspringBug,bugOffspringBug,population.bug);
//		Save2DBins(outbugOffspringPred,bugOffspringPred,population.bug);
//		
//		Save2DBins(outbugFoodBug,bugFoodBug,population.bug);
//		Save2DBins(outbugFoodPred,bugFoodPred,population.bug);
//		Save2DBins(outbugBugPred,bugBugPred,population.bug);
//	}
//	void Save2DBins(ofstream& file, TwoDBins inBins, int pop){
//		for(int i = 0; i < inBins.bins.size(); i++){
//			file << iteration << '\t' << (inBins.bins[i].xMin + inBins.bins[i].xMax)/2 << '\t' << (inBins.bins[i].yMin + inBins.bins[i].yMax)/2 << '\t' << double(inBins.bins[i].frequency)/double(pop) << endl;
//		}
//	}
//	void SaveEntityPosition(ofstream& file){
//		for (const auto &entitya : entities){
//			file << iteration << "\t" << entitya->type << "\t" << entitya->getx() << "\t" << entitya->gety() << endl;
//		}
//	}
//	void SavePopulation(ofstream& file){
//		file << iteration << "\t" << population.food << "\t" << population.bug << "\t" << population.predator << endl;
//	}
	void UpdateFood(){
		switch(setting->foodRegeneration){
//          case Settings::FoodEvoType::Random:
//				RandomDropFood();
//				break;
//			case Settings::FoodEvoType::Growth:
//				GrowFood();
//				break;
//			case Settings::FoodEvoType::Discrete:
//				DiscreteDropFood();
//				break;
//			case Settings::FoodEvoType::SpaceChange:
//				SpaceSeparatedFood();
//				break;
//			case Settings::FoodEvoType::TimeChange:
//				TimeSeparateFood();
//				break;
            case Settings::FoodEvoType::SolveNutrientField:
                SolveNutrientField();
                break;
			default:
				break;
		}
	}
    
    void SolveNutrientField(){
//        GrowFood(175);
//        int NumSites;
//        NumSites = Mod(abs(int(generator()))  , int(0.002*setting->systemX * setting->systemY));
//        for (int i = 0; i < NumSites; i++){
//            CreateFood();
//        }
    }
    
//	void SpaceSeparatedFood(){
//			int NumSites;
//			NumSites = Mod(abs(int(generator()))  , int(0.01*setting->systemX * setting->systemY));
//			for (int i = 0; i < NumSites; i+=3){
//				if(Mod(abs(int(generator())),11) == 0){//Left side
//					CreateFoodLeft();
//				}
//				else{//right side
//					CreateFoodRight();
//				}
//			}
//			//growth mechanicd
//			GrowFood(175);	
//	}
//	void TimeSeparateFood(){//change at 1000
//		if(iteration <= 1000){
//			int NumSites;
//			NumSites = Mod(abs(int(generator()))  , int(0.01*setting->systemX * setting->systemY));
//			for (int i = 0; i < NumSites; i+=3){
//				CreateNutrientField();
//			}
//			GrowFood(175);	
//		}
//		else{//new food growth values
//			int NumSites;
//			NumSites = Mod(abs(int(generator()))  , int(0.005*setting->systemX * setting->systemY));
//			for (int i = 0; i < NumSites; i+=3){
//				CreateFood();
//			}
//			GrowFood(75);	
//		}
//	}
//	void DiscreteDropFood(){
//		for(int i = 0; i < 100; i++){
//			CreateFood(setting->systemX*(i%10)/10 - 2 + Mod(abs(int(generator())),5),setting->systemY*(i%10)/10 - 2 + Mod(abs(int(generator())),5));
//		}
//		GrowFood(175);	
//	}
//	void GrowFood(int chance){//Chance in 10000
//		const vector<Entity*> entitiesCopy = entities;
//		for (const auto &entitya : entitiesCopy){
//			if (entitya->type == Entity::EntityType::Food){
//				if (Mod(abs(int(generator()))  , 10000) <= chance){ // chance for growth
//					CreateFood(entitya->getx() + (Mod(abs(int(generator()))  , 3) - 1), entitya->gety() + (Mod(abs(int(generator()))  , 3) - 1));
//				}
//			}
//		}
//	}
//	void GrowFood(){
//		GrowFood(175);		
//		int NumSites;
//		NumSites = Mod(abs(int(generator()))  , int(0.002*setting->systemX * setting->systemY));
//		for (int i = 0; i < NumSites; i++){
//			CreateFood();
//		}
//	}
//	void RandomDropFood(){
//		int NumSites;
//		NumSites = Mod(abs(int(generator()))  , int(0.01*setting->systemX * setting->systemY));
//		for (int i = 0; i < NumSites; i+=3){
//			CreateFood();
//		}
//		
//	}
    
	void DisplayEntities(){
		for (const auto &entitya : entities){
            if (entitya->getType() == Entity::EntityType::Bacterium){
                float x = entitya->getx();
                float y = entitya->gety();
                Display.drawimg(int(entitya->type), x, y);
            }
		}
		Display.disp();
	}
//	void PrintEntities(){
//		//char c = ' ';
//		//Clear array
//		for(int i = 0; i < setting->systemX * setting->systemY; ++i){
//			screen[i] = ' ';
//		}
//		
//		//Fill array with entities
//		for (const auto &entitya : entities){
//			//cout<< GetEnumString(entitya->type) << " (" << entitya->getx() << "," << entitya->gety() << ")" << endl;
//			switch(entitya->type){
//				case Entity::EntityType::NutrientField:
//					screen[(entitya->gety()*setting->systemX) + entitya->getx()] = 'F';
//					break;
//				case Entity::EntityType::Bacterium:
//					screen[(entitya->gety()*setting->systemX) + entitya->getx()] = 'B';
//					break;
////				case Entity::EntityType::Predator:
////					screen[(entitya->gety()*setting->systemX) + entitya->getx()] = 'P';
////					break;
//				default:
//					break;
//			}
//		}
//		//print array to screen
//		for(int i = 0; i < setting->systemY; i++){
//			for(int j = 0; j < setting->systemX; j++){
//				std::cout << screen[setting->systemX*i + j];
//			}
//			std::cout << "|" << endl;
//		}
//		std::cout << endl;
//		return;
//	}
#pragma endregion simulation iteration
#pragma region  file strings
	std::string GetPath(FileType type){
		std::stringstream path;
		path << "Results" << "\\" << GetEnumString(type) << "\\" << setting->systemX << "x" << setting->systemY << "\\" << GetFDEnumString() << "\\" << GetFEEnumString();
		return path.str();
	}
	std::string GetFileName(){
		std::stringstream filename;
		filename << setting->systemX << "x" << setting->systemY;
		return filename.str();
	}
	std::string GetFileExtention(FileType type){
		switch(type){
			case Text:
				return ".txt";
				break;
			case Video:
				return ".avi";
				break;
			default:
				return "";
				break;
		}
	}
	std::string GetFDEnumString(){
		switch (setting->foodDistribution){
		case Settings::FoodDistType::NoDist:
				return "None";
				break;
			case Settings::FoodDistType::Single:
				return "Single";
				break;
			case Settings::FoodDistType::Few:
				return "Few";
				break;
			case Settings::FoodDistType::Many:
				return "Many";
				break;
			default:
				return "";
				break;
		}
	}
	std::string GetFEEnumString(){
		switch (setting->foodRegeneration){
			case Settings::FoodEvoType::NoEvo:
				return "None";
				break;
			case Settings::FoodEvoType::Random:
				return "Random";
				break;
			case Settings::FoodEvoType::Growth:
				return "Growth";
				break;
			default:
				return "";
				break;
		}
	}
	std::string GetEnumString(Entity::EntityType Type){
		switch (Type){
			case Entity::EntityType::Bacterium:
				return "Bacterium";
				break;
            case Entity::EntityType::NutrientField:
				return "NutrientField";
				break;
//			case Entity::EntityType::Predator:
//				return "Predator";
//				break;
			default:
				return "";
				break;
		}
	}
	std::string GetEnumString(FileType Type){
		switch (Type){
			case Text:
				return "Text";
				break;
			case Video:
				return "Video";
				break;
			default:
				return "";
				break;
		}
	}
	string AppendToString(string S1, string S2){ //creates a filename string showing some properties of the system
		ostringstream  combinedString;
		combinedString  << S1 << S2;
		return combinedString.str();
	}
#pragma endregion Dynamic string creation for files
};
#endif
