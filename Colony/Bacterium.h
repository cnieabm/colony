//
//  Bug.h
//  compevol
//
//  Created by Michael Michaelides on 15/01/2014.
//  Copyright (c) 2014 Comp Evol. All rights reserved.
//

#ifndef compevol_Bacterium_h
#define compevol_Bacterium_h
#include "Mob.h"

class Bacterium: public Mob{
protected:
    // Model Parameters
    double A, C, E;
    
public:
	
	//constructor
	Bacterium(float a, float b, Settings* simSetting){
		setting = simSetting;
        
        //Model parameters
        A = setting -> A;
        C = setting -> C;
        E = setting -> E;
        
        pos << a,
                b;
        posnew = pos;
        if (a<0.5){
            cout<<"problem"<<endl;
        }
        vel<<1.,
            1.;
		vision = 3.0;
		alive=true;
        active=true;
		reproduce=false;
		generation = 1;
		mass = 0.0005;
        massChange = 0;
        radius = 1;
		RepThreshold = 1.;
//		combatValue = 1.0;
		AvgOffspring = 1.0;
		massChange = 0.0;
		statfoodit = 0.5;
//		foodCap = -10.0;
//		digestit = 5.0;
//		foodTransferRate = 5.0;
//		mutPrey=0.0;
//		mutPred=0.0;
//		mutSame=0.0;
        stepAngle = setting -> stepangle; //the MFP (total dist travelled at an angle before choosing a new one)
        distSameAngle = 0; //distance travelled at current angle
		type = EntityType::Bacterium;
//		pattern.push_back(move(1,0));
	}
	
	//reproduction constructor
	Bacterium(Entity *ancestor, Settings* simSettings){
		setting = simSettings;
		//determines position of offspring
        pos = ancestor -> getPosition();
        vel = -1.*(ancestor -> getVelocity());
//        if (getrand()>0.5){
//            pos(0)+=0.01;
//        } else{
//            pos(0)-=0.01;
//        }
//        if (getrand()>0.5){
//            pos(1)+=0.01;
//        } else{
//            pos(1)-=0.01;
//        }
//        cout<<pos<<endl;
//        cout<<endl;
//		if(!setting->explosiveRepoduction){
//			ancestor->PopFreeSite(x,y);
//		}
		
		alive=true;
		reproduce = false;
        mass = ancestor->GetMass();
		generation = ancestor->GetGeneration() + 1;
		vision = ancestor->GetVision();
		statfoodit = ancestor->GetStatfoodit();
		combatValue = ancestor->GetCombatValue();
//		foodCap = ancestor->GetFoodCap();
//		digestit = ancestor->GetDigestIt();
//		foodTransferRate = ancestor->GetTransferRate();
        stepAngle = ancestor->GetStepAngle(); //the MFP (total dist travelled at an angle before choosing a new one)
        distSameAngle = stepAngle; //distance travelled at current angle
		
		//here mutation algorithm must be introduced to cause minor changes to the values
		RepThreshold = ancestor->GetRepThreshold();
		AvgOffspring = ancestor->GetAvgOffspring();
//		mutPred = mutation(ancestor->GetMutPred(),0.05);
//		mutPrey = mutation(ancestor->GetMutPrey(),0.05);
//		mutSame = mutation(ancestor->GetMutSame(),0.05);
		
		type = EntityType::Bacterium;
//		pattern = ancestor->GetMoveList();
        posnew = pos;
        // Model Parameters
        A = ancestor->GetParameterA();
        C = ancestor->GetParameterC();
        E = ancestor->GetParameterE();
        
        massChange = 0.0;
        radius=1.;
	}
	
	void PrepareMove(Entity * nutrient_field) override{
//		nearest.Reset();

		if (mass>RepThreshold){
//			for (const auto &entitya : entities){
//				if (((x - entitya->getx()) == 1) && (y - entitya->gety() == 0)){
//					nearest.leftOccupied = true;
//				}
//				else if (((x - entitya->getx()) == -1) && (y - entitya->gety() == 0)){
//					nearest.rightOccupied = true;
//				}
//				else if (((x - entitya->getx()) == 0) && (y - entitya->gety() == 1)){
//					nearest.downOccupied = true;
//				}
//				else if (((x - entitya->getx()) == 0) && (y - entitya->gety() == -1)){
//					nearest.upOccupied = true;
//				}
//			}
			
//			if(nearest.FreeSite() || setting->explosiveRepoduction){
//            if(setting->explosiveRepoduction){
				reproduce = true;
//			}
		}
        if (!reproduce) {
            double Y = nutrient_field->GetMass(pos);
            if (mass>0){
                massChange = C * Y/(Y+A)*(1.+mass)- E*vel.norm();
            }
            else{
                mass = 0.0005;
                massChange = C * Y/(Y+A)*(1.+mass);
            }

            // Determining new position
            vel *= 1./(vel.norm()*sqrt(1+mass));
            posnew = pos + vel * nutrient_field->GetTimestep();

            distSameAngle += (posnew-pos).norm();
            
            if (distSameAngle>=stepAngle){
                rot = Rotation2Df((float)(getrand()*2.*M_PI));
                vel = rot * vel;
                distSameAngle = 0;
            }
		}
	}
    
    int GetNumOffspring() override
    {
        return AvgOffspring;
    }
    
    // getters for model parameters
    double GetParameterA() {return A;}
    double GetParameterC() {return C;}
    double GetParameterE() {return E;}
    
    // getter for mass change
    double GetMassChange() override {return massChange;}
	
	~Bacterium(){};
};

#endif
// In prepare move, finding best move. no longer relevant.

//			int xpred=0, ypred=0, xsame=0, ysame=0, xprey=0, yprey=0;
//			int bestpos=8;
//			double bestscore;
//			int distX, distY;
//			int entx, enty;
//			int i, j, c;
//			moveList rotpattern;
//			possList possmoves;

//			for (c=0; c<pattern.size(); c++){
//				rotpattern.push_back(pattern[c]);
//				for (i=0; i<9; i++){
//					xnew=x;
//					ynew=y;
//					xpred=0; ypred=0; xprey=0; yprey=0; xsame=0; ysame=0;
//
//					if (i!=8){
//						for (j=0; j<rotpattern.size(); j++){
//							xnew+=rotpattern[j].first;
//							ynew+=rotpattern[j].second;
//							rotpattern[j]=move(rotpattern[j].second,(-1)*(rotpattern[j].first));
//							if (i==3){
//								rotpattern[j].second*=(-1);
//							}
//						}
//					}
//
//					possmoves.push_back(PossMove(Mod(xnew,setting->systemX), Mod(ynew,setting->systemY), amountOfFood, combatValue,setting->systemX,setting->systemY, mutPred, mutPrey, mutSame));
//					for (const auto &entitya : entities){
//						entx=entitya->getx();
//						enty=entitya->gety();
//
//						if (abs(entx - x) < Mod(abs(entx + x), setting->systemX)){
//							distX = abs(entx - x);
//						}
//						else{
//							distX = Mod(abs(entx + x), setting->systemX);
//						}
//
//						if (abs(enty - y) < Mod(abs(enty + y), setting->systemY)){
//							distY = abs(enty - y);
//						}
//						else{
//							distY = Mod(abs(enty + y), setting->systemY);
//						}
//
//						if ((distX * distX + distY * distY) <= pow(vision, 2)){ //Can Interact with
//							switch (entitya->getType()){
//								case EntityType::Bug:
//									possmoves[c*8+i].SameAt(entx, enty);
//									break;
//								case EntityType::Food:
//									possmoves[c*8+i].PreyAt(entx, enty);
//									break;
//								case EntityType::Predator:
//									possmoves[c*8+i].PredAt(entx, enty);
//									break;
//							}
//						}
//					}
//				}
//			}

//			bestscore=possmoves[8].getscore();

//			for (c=0; c<pattern.size(); c++){
//				for (i=0; i<8; i++){
//					possmoves[i].posClear = true;
//					if(setting->stacking){
//						for (const auto &entitya : entities){
//							if (possmoves[i].getx() == entitya->getx() && possmoves[i].gety() == entitya->gety() && entitya->getType() != EntityType::Food){
//								possmoves[i].posClear = false;
//							}
//						}
//					}
//					if(possmoves[i].posClear){
//						if (possmoves[c*8+i].getscore()>bestscore){
//							bestpos = c*8+i;
//							bestscore = possmoves[c*8+i].getscore();
//						}
//						else if ((possmoves[c*8+i].getscore() == bestscore) && (getrand()>0.5)){
//							bestpos = c*8+i;
//							bestscore = possmoves[c*8+i].getscore();
//						}
//					}
//				}
//			}
//			posnew(0)=possmoves[bestpos].getx();
//			posnew(1)=possmoves[bestpos].gety();
//			for (const auto &entitya : entities){
//				if ((entitya->getType() == EntityType::NutrientField)&&(entitya->alive == true)){
//					if (posnew==entitya->getpos()){
//						//cout << "Food Eaten" << endl;
//						Eat(entitya);
//						break;
//					}
//				}
//			}