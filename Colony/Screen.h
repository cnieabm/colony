//
//  Screen.h
//  compevol
//
//  Created by Michael Michaelides on 29/01/2014.
//  Copyright (c) 2014 Comp Evol. All rights reserved.
//

#ifndef compevol_Screen_h
#define compevol_Screen_h

#include <SDL2/SDL.h>

class Screen{
	int SCREEN_WIDTH;
	int SCREEN_HEIGHT;
    double scale;
	
	SDL_Texture *background;
	SDL_Texture *food;
	SDL_Texture *bug;
	SDL_Texture *pred;
	
	SDL_Renderer *renderer;
	SDL_Window *window;
public:
	Screen(){}

	Screen(int x, int y){
        
		SCREEN_WIDTH  = 1000;
		SCREEN_HEIGHT = 1000;
        
        scale = SCREEN_WIDTH/x;

		//initialising
		if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
			logSDLError(std::cout, "SDL_Init");
			//return 1;
		}
		
		//creating window and renderer
		window = SDL_CreateWindow("Habitat", 100, 100, SCREEN_WIDTH,
											  SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (window == nullptr){
			logSDLError(std::cout, "CreateWindow");
			//return 2;
		}
		renderer = SDL_CreateRenderer(window, -1,
													SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
		if (renderer == nullptr){
			logSDLError(std::cout, "CreateRenderer");
			//return 3;
		}
		
		//loading textures
        std::string respath = "/Users/michalis/Documents/UCL/ABM/Colony/Colony/res/";
		background = loadTexture(respath+"Empty.bmp", renderer);
		food = loadTexture(respath+"Food.bmp", renderer);
		bug = loadTexture(respath+"Bug.bmp", renderer);
		pred = loadTexture(respath+"Predator.bmp", renderer);
		
//		if (background == nullptr || food == nullptr || bug == nullptr || pred == nullptr)
//			return 4;
		
		//drawing background
		SDL_RenderClear(renderer);
		
		int bW, bH;
		SDL_QueryTexture(background, NULL, NULL, &bW, &bH);
		
		for (int i=0; i<SCREEN_WIDTH; i+=bW){
			for (int j=0; j<SCREEN_HEIGHT; j+=bH){
				renderTexture(background, renderer, i, j);
			}
		}
	}
	
	void disp(){
		//present and delay
		SDL_RenderPresent(renderer);
//		SDL_Delay(2000);
		
		//drawing background
		SDL_RenderClear(renderer);
		
		int bW, bH;
		SDL_QueryTexture(background, NULL, NULL, &bW, &bH);
		
		for (int i=0; i<SCREEN_WIDTH; i+=bW){
			for (int j=0; j<SCREEN_HEIGHT; j+=bH){
				renderTexture(background, renderer, i, j);
			}
		}
		
	}
	
	void end(){
		//cleaning up
		SDL_DestroyTexture(background);
		SDL_DestroyTexture(food);
		SDL_DestroyTexture(bug);
		SDL_DestroyTexture(pred);
		SDL_DestroyRenderer(renderer);
		SDL_DestroyWindow(window);
		SDL_Quit();

	}
	
	void drawimg(int type, float x, float y){
		//drawing foregrounds
		int iW, iH;
		
		SDL_QueryTexture(food, NULL, NULL, &iW, &iH);
		SDL_QueryTexture(bug, NULL, NULL, &iW, &iH);
		SDL_QueryTexture(pred, NULL, NULL, &iW, &iH);
		
		x=round(x*scale) - iW/2;
		y=round(y*scale) - iH/2;
		
		switch (type){
			case 0:
				renderTexture(food, renderer, x, y);
				break;
			case 1:
				renderTexture(bug, renderer, x, y);
				break;
//			case 2:
//				renderTexture(pred, renderer, x, y);
//				break;
			default:
				break;
		}
	}
private:
	/**
	 * Log an SDL error with some error message to the output stream of our choice
	 * @param os The output stream to write the message too
	 * @param msg The error message to write, format will be msg error: SDL_GetError()
	 */
	void logSDLError(std::ostream &os, const std::string &msg){
		os << msg << " error: " << SDL_GetError() << std::endl;
	}
	
	/**
	 * Loads a BMP image into a texture on the rendering device
	 * @param file The BMP image file to load
	 * @param ren The renderer to load the texture onto
	 * @return the loaded texture, or nullptr if something went wrong.
	 */
	SDL_Texture* loadTexture(const std::string &file, SDL_Renderer *ren){
		//Initialize to nullptr to avoid dangling pointer issues
		SDL_Texture *texture = nullptr;
		//Load the image
		SDL_Surface *loadedImage = SDL_LoadBMP(file.c_str());
		//If the loading went ok, convert to texture and return the texture
		if (loadedImage != nullptr){
			texture = SDL_CreateTextureFromSurface(ren, loadedImage);
			SDL_FreeSurface(loadedImage);
			//Make sure converting went ok too
			if (texture == nullptr)
				logSDLError(std::cout, "CreateTextureFromSurface");
		}
		else
			logSDLError(std::cout, "LoadBMP");
		
		return texture;
	}
	
	/**
	 * Draw an SDL_Texture to an SDL_Renderer at position x, y, preserving
	 * the texture's width and height
	 * @param tex The source texture we want to draw
	 * @param ren The renderer we want to draw too
	 * @param x The x coordinate to draw too
	 * @param y The y coordinate to draw too
	 */
	void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y){
		//Setup the destination rectangle to be at the position we want
		SDL_Rect dst;
		dst.x = x;
		dst.y = y;
		//Query the texture to get its width and height to use
		SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
		SDL_RenderCopy(ren, tex, NULL, &dst);
	}
};


#endif
