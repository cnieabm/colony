//
//  Colour.h
//  compevol
//
//  Created by Michael Michaelides on 15/01/2014.
//  Copyright (c) 2014 Comp Evol. All rights reserved.
//

#ifndef compevol_Colour_h
#define compevol_Colour_h

class Colour{
public:
	float R,G,B;
	Colour(){
		R = 0.0;
		G = 0.0;
		B = 0.0;
	}
	Colour(float r, float g, float b){
		R = r;
		G = g;
		B = b;
	}
};

#endif
