//
//  Settings.h
//  compevol
//
//  Created by Michael Michaelides on 19/03/2014.
//  Copyright (c) 2014 Comp Evol. All rights reserved.
//

#ifndef compevol_Settings_h
#define compevol_Settings_h

#include <fstream>
#include <iostream>
using namespace std;

class Settings{
public:
	//type definitions: these are also used in simulation
	enum FoodDistType {NoDist, Single, Few, Many, NutrientField}; // how the food is placed initially
	enum FoodEvoType {NoEvo, Random, Growth, Discrete, TimeChange, SpaceChange, SolveNutrientField}; // how the food is replenished
	
	int systemX;
	int systemY;
	
	bool predators;
	bool stacking;
	bool evolution;
	bool instantConsume;
	bool explosiveRepoduction;
	
	int predatorExtinction;
	double foodCap;
	double predatorBonus;
	double deathChance;
    double timestep;
		
	string path;
				
	FoodDistType foodDistribution;
	FoodEvoType foodRegeneration;
	
    //the MFP (total dist travelled at one angle before changing)
    double stepangle;
    
    //Model parameters
    double A;
    double B;
    double C;
    double D;
    double E;
    
    double cellsize;

	Settings(string setpath){
		ifstream inpath;
		//string set;
		inpath.open(setpath);
		string value;
		int i=1;
		if (inpath.is_open()){
			while (!inpath.eof()){
				//inpath>>set;
				getline(inpath, value, ';');
				assign(i, value);
				
				getline(inpath, value, '\n'); //read and ignore rest of line (comments)
				i++;
			}
			inpath.close();
		}
	}
	
	void OutSettings(){
		cout<<"systemX: "<<systemX<<endl<<
			"systemY: "<<systemY<<endl<<
			"predators: "<<predators<<endl<<
			"stacking: "<<stacking<<endl<<
			"evolution: "<<evolution<<endl<<
			"instantConsume: "<<instantConsume<<endl<<
			"explosiveRepoduction: "<<explosiveRepoduction<<endl<<
			"predatorExtinction: "<<predatorExtinction<<endl<<
			"foodCap: "<<foodCap<<endl<<
			"predatorBonus: "<<predatorBonus<<endl<<
			"deathChance: "<<deathChance<<endl<<
			"foodDistribution: "<<foodDistribution<<endl<<
			"foodRegeneration: "<<foodRegeneration<<endl<<
            "timestep: "<<timestep<<endl<<
            "path: "<< path<<endl<<
            "stepangle: "<< stepangle<<endl<<
            "A: "<<A<<endl<<
            "B: "<<B<<endl<<
            "C: "<<C<<endl<<
            "D: "<<D<<endl<<
            "E: "<<E<<endl<<
            "cellsize: "<<cellsize<<endl;
	}
	
private:
	FoodDistType foodDType(string &str){
		if (str=="None")
			return FoodDistType::NoDist;
		else if (str =="Single")
			return FoodDistType::Single;
		else if (str =="Few")
			return FoodDistType::Few;
		else if (str =="Many")
			return FoodDistType::Many;
        else if (str =="NutrientField")
            return FoodDistType::NutrientField;
		else{
			cout<<"There has been an error loading the foodDistType for the simulation."<<endl;
			return FoodDistType::NoDist;
		}
	}
	
	FoodEvoType foodEType(string &str){
		if (str=="None")
			return FoodEvoType::NoEvo;
		else if (str =="Random")
			return FoodEvoType::Random;
		else if (str =="Growth")
			return FoodEvoType::Growth;
		else if (str =="Discrete")
			return FoodEvoType::Discrete;
		else if (str =="Space")
			return FoodEvoType::SpaceChange;
		else if (str =="Time")
			return FoodEvoType::TimeChange;
        else if (str =="SolveNutrientField")
            return FoodEvoType::SolveNutrientField;
		else{
			cout<<"There has been an error loading the foodEvoType for the simulation."<<endl;
			return FoodEvoType::NoEvo;
		}
	}
	
	void assign(int i, string& value){
		switch (i){
			case 1:
				systemX = atoi(value.c_str());
				break;
			case 2:
				systemY = atoi(value.c_str());
				break;
			case 3:
				predators = (bool)atoi(value.c_str());
				break;
			case 4:
				stacking = (bool)atoi(value.c_str());
				break;
			case 5:
				evolution = (bool)atoi(value.c_str());
				break;
			case 6:
				instantConsume = (bool)atoi(value.c_str());
				break;
			case 7:
				explosiveRepoduction = (bool)atoi(value.c_str());
				break;
			case 8:
				predatorExtinction = atoi(value.c_str());
				break;
			case 9:
				foodCap = atof(value.c_str());
				break;
			case 10:
				predatorBonus = atof(value.c_str());
				break;
			case 11:
				deathChance = atof(value.c_str());
				break;
			case 12:
				foodDistribution = foodDType(value);
				break;
			case 13:
				foodRegeneration = foodEType(value);
				break;
            case 14:
                timestep = atof(value.c_str());
			case 15:
				path = value;
				break;
            case 16:
                stepangle = atof(value.c_str());
                break;
            case 17:
                A = atof(value.c_str());
                break;
            case 18:
                B = atof(value.c_str());
                break;
            case 19:
                C = atof(value.c_str());
                break;
            case 20:
                D = atof(value.c_str());
                break;
            case 21:
                E = atof(value.c_str());
                break;
            case 22:
                cellsize = atof(value.c_str());
			default:
				break;
		}
	}
	
};

#endif
