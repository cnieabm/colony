//
//  Entity.h
//  compevol
//
//  Created by Michael Michaelides on 15/01/2014.
//  Copyright (c) 2014 Comp Evol. All rights reserved.
//

#ifndef compevol_Entity_h
#define compevol_Entity_h
#include <vector>
#include <math.h>
#include <random>
#include <eigen3/Eigen/Dense>
#include "Settings.h"

using namespace Eigen;
using namespace std;

static random_device seed;// seed value

class Entity{
protected:
	double amountOfFood;
//	typedef pair<int, int> move;
//	typedef vector<move> moveList;
	Settings* setting;
    Vector2f pos;
    Vector2f vel;
    Vector2f posnew;
    Vector2f velnew;
    Rotation2Df rot;
    
#pragma region Utility Functions
	int Mod(float A, float B){
		if(A<0){
            return Mod((float)(A+B),(float)B);
		}
		else{
			return (int)round(A)%(int)round(B);
		}
	}
	
	double getrand(){
		return (double(generator())/double(generator.max()));
	}
	
	double mutation(double m, double s){
		if(setting->evolution){
			double z,y1, y2;
		
			do{
				y1=-log(getrand());
				y2=-log(getrand());
			}while (!(y2>=(y1-1)*(y1-1)*0.5));
		
			z=y1;
		
			if (getrand()>0.5)
				z*=-1;
			return z*s+m;
		}
		else{
			return m;
		}
	}
#pragma endregion
	
public:
	enum EntityType{Bacterium, NutrientField};
	EntityType type;
	bool alive;
	bool reproduce;
	bool active;
	mt19937_64 generator; //[README] 64bit machines only! If you have 32bit change mt19937_64 to mt19937
	
	//constructor
	Entity(){//used for inheritance only
		generator.seed(seed());
		active=true;
	}
	Entity(Settings* simSettings){
		setting = simSettings;
		generator.seed(seed());
		active=true;
	}
	
	virtual void update(vector <Entity*>& entities){}
    virtual void update_entity_list(vector <Entity*>& entities){}
    virtual void print_field(string outpath){}
	virtual void PrepareMove(Entity * nutrient_field){}
    virtual void PrepareMove(){}
	virtual double GetMass(Vector2f pos){return 0;}
    virtual double GetMass(){return 0;}
	virtual void haveReproduced(int num){}
	virtual double GetRepThreshold(){return 0.0;}
	virtual double GetCombatValue(){return 0.0;}
	virtual bool GetRepoduce(){ return false; }
	virtual void PopFreeSite(int& x, int& y){}
	virtual int GetGeneration(){return 0;}
	virtual double GetVision(){return 1.0;}
	virtual double GetAvgOffspring(){return 2.0;}
	virtual int GetNumOffspring(){return 2;}
	virtual double GetStatfoodit() {return 0.5;}
	virtual double GetMovefoodit() {return 1.0;}
	virtual double GetMutPred() {return 1.0;}
	virtual double GetMutPrey() {return 1.0;}
	virtual double GetMutSame() {return 1.0;}
	virtual double GetTransferRate() {return 1.0;}
	virtual double GetFoodCap() {return 1.0;}
	virtual double GetDigestIt() {return 1.0;}
    virtual double GetStepAngle(){return 1.0;}
    virtual double GetDistSameAngle(){return 1.0;}
    // getters for model parameters
    virtual double GetParameterA() {return 1.0;}
    virtual double GetParameterC() {return 1.0;}
    virtual double GetParameterE() {return 1.0;}
    // getter for mass change
    virtual double GetMassChange() {return 0;}
    virtual double GetTimestep() {return 0.;}
    virtual void test_initial() {}
//	virtual moveList GetMoveList(){
//		moveList a;
//		a.push_back(move(0,0));
//		return a;
//	}
	
	bool IsActive(){
		return active;
	}
	
//	void IsCaught(){
//		active=false;
//	}
    
    float getx(){
//        if pos(0)>setting->systemX{
            // return Mod(pos(0), (float)setting->systemX);
//        }
//        else{
//            return floor(pos(0)+0.5);
//        }
        return pos[0];
    }
    
    float gety(){
        //return Mod(pos(1), (float)setting->systemY);
        return pos[1];
    }
    
    void setx(float a){
        pos(0) = a;
    }
    
    void sety(float b){
        pos(1) = b;
    }
    
    Vector2f getPosition(){
        return pos;
    }
    
    void setPosition(Vector2f vpos){
        pos = vpos;
    }
    
    Vector2f getVelocity(){
        return vel;
    }
    
    void setVelocity(Vector2f vvel){
        vel = vvel;
    }
	
	void RemoveFood(double amount){
		amountOfFood-=amount;
	}
	
	EntityType getType(){
		return type;
	}
    void Inactivate(){
        active = false;
    }
    
	void Kill(){
		alive = false;
		//let the simulation class deal with removing the entity
	}
	
	virtual ~Entity(){};
};

#endif
