//
//  NutrientField.h
//  Colony
//
//  Created by Michalis Michaelides on 02/02/2015.
//  Copyright (c) 2015 UCL. All rights reserved.
//

#ifndef Colony_NutrientField_h
#define Colony_NutrientField_h
#include "Entity.h"
#include<iostream>
#include<fstream>
#include<stdio.h>
#include<string>
#include<stdlib.h>
#include<math.h>
#include<time.h>

using namespace std;

class NutrientField : public Entity{
#pragma region Utility Functions
    int Mod(int A, int B){
        if(A<0){
            return Mod(A+B,B);
        }
        else{
            return A%B;
        }
    }
    
    inline double sq(double a){
        return a*a;
    }
    
private:

#pragma endregion
protected:
    int siz;
    int splits;
    double avg;
    
    double** grid;
    double** grid_n;
    int** grid_ent;
    
    double dx;
    double dt;
    double time;
    double nsteps;
    
    double A;
    double B;
    double D;
    
    vector<Entity*> entities;
public:
    
    NutrientField(Settings* simSettings){
        // int argc, const char* const* argv,
        setting = simSettings;
//        x = Mod(X,setting->systemX);
//        y = Mod(Y,setting->systemY);
        type = EntityType::NutrientField;

        alive=true;
        amountOfFood=25.0;
        siz = setting->systemX;
        splits = siz*(1./setting->cellsize);
        
        // Model parameters
        A = simSettings -> A;
        B = simSettings -> B;
        D = simSettings -> D;
        
        avg = 0;
        grid = new double*[splits];
        grid_n = new double*[splits];
        grid_ent = new int*[splits];
        for (int i=0;i<splits;i++){
            grid[i]=new double[splits];
            grid_n[i]=new double[splits];
            grid_ent[i]=new int[splits];
        }
        
        dx=setting->cellsize;
        dt=sq(dx)/(8*D);
        cout<<"dt: "<<dt<<endl;
//        time=0.5*sq(M_PI)/D;
//        nsteps=time/dt;
        
        // Applying Boundary Conditions
        setBC();
        cout<<"Size is"<<siz<<endl;
        
        for(int i=0;i<splits;i++){
            for(int j=0;j<splits;j++){
                grid_n[i][j]=grid[i][j];
            }
        }

    }
    
    void setBC(){
        //Set boundary conditions - fixed
        for(int i=0;i<splits;i++){
            for(int j=0;j<splits;j++){
                grid[i][j]=1.;
            }
        }
        
//        for (int i=0;i<splits;i++){
//            grid[i][0]=1.;
//            grid[i][splits-1]=1.;
//            grid[0][i]=1.;
//            grid[splits-1][i]=1.;
//            
//        }
    }
    
    double GetMass(Vector2f ent_pos) override{
//        Point loc(0.5, 0.5);
//        double val;
//        val = (double) t_system->point_value(0, loc, false);
//        mesh->clear_point_locator();
        double i = ent_pos(0)*splits/siz;
        double j = ent_pos(1)*splits/siz;
        float mass; // = grid[(int)round(i)][(int)round(j)];
        mass = 0.;
        mass += grid[(int)floor(i)][(int)floor(j)];
        mass += grid[(int)floor(i)][(int)ceil(j)];
        mass += grid[(int)ceil(i)][(int)floor(j)];
        mass += grid[(int)ceil(i)][(int)ceil(j)];
        mass *= sq(1./2.);
        
//        int c = 0;
//        Vector2f ent_pos_o;
//        for (const auto &entitya : entities){
//            if (entitya->getType() == EntityType::Bacterium){
//                ent_pos_o = entitya->getPosition();
//                int i_o = round(ent_pos_o(0)*splits/siz);
//                int j_o = round(ent_pos_o(1)*splits/siz);
//                if (i_o==(int)round(i) && j_o==(int)round(j)){
//                    c++;
//                }
//            }
//        }
//        if (c==0){
//            cout<<"it's zero"<<endl;
//        }
//        mass/=(float)c;

        return mass;
    }
//
    void update_entity_list(vector<Entity*> & ents) override{
        entities = ents;
    }
    void update(vector<Entity*> & ents) override
    {
        // Interpolating agents into grid
        entities = ents;
        Vector2f ent_pos;
        for(int i=0;i<splits;i++){
            for(int j=0;j<splits;j++){
                grid_ent[i][j]=0;
            }
        }
        
        // finding how many agents in the grid
        int i_o;
        int j_o;
        for (const auto &entitya : ents){
            if (entitya->getType() == EntityType::Bacterium){
                //                        ent_mass = entitya->GetMassChange();
                ent_pos = entitya->getPosition();
                i_o = (int)floor(ent_pos(0)*splits/siz);
                j_o = (int)floor(ent_pos(1)*splits/siz);
                grid_ent[i_o][j_o] += 1;
                grid_ent[i_o][j_o+1] += 1;
                grid_ent[i_o+1][j_o] += 1;
                grid_ent[i_o+1][j_o+1] += 1;
            }
        }
        
        // FTCS and forward Euler
        for(int i=1;i<splits-1;i++){
            for(int j=1;j<splits-1;j++){
                grid_n[i][j] = grid[i][j] + dt*(D*(grid[i-1][j]+grid[i+1][j]+grid[i][j-1]+grid[i][j+1]-4*grid[i][j])/sq(dx) - grid_ent[i][j]*sq(1./(2.*dx)) * B * grid[i][j] / (grid[i][j] + A));
                if (grid_n[i][j]<0){
                    grid_n[i][j] = 0.;
                }
            }
        }
        
        // Enforcing dY/dx = 0, dY/dy = 0 (no flow conditions)
        for(int i=0;i<splits;i++){
            grid_n[0][i]=grid_n[2][i];
            grid_n[splits-1][i]=grid_n[splits-3][i];
        }
        
        for(int i=0;i<splits;i++){
            grid_n[i][0]=grid_n[i][2];
            grid_n[i][splits-1]=grid_n[i][splits-3];
        }
        
        // Copying new grid into old
        for(int i=0;i<splits;i++){
            for(int j=0;j<splits;j++){
                grid[i][j]=grid_n[i][j];
            }
        }
        
        // Calculating average temperature
        avg = 0;
        for(int i=1;i<splits-1;i++){
            for(int j=1;j<splits-1;j++){
                avg+=grid[i][j];
            }
        }
        avg/=(splits-2)*(splits-2);
        cout<<avg<<endl;
    }
    
    double GetTimestep () override
    {
        return dt;
    }

    void test_initial() override {
        siz = 4;
        splits = siz*10;
        D = 1.;
        dx=(double)siz/(double)splits;
        dt=sq(dx)/(8*D);
        float mean1 = 2.;
        float mean2 = 2.;
        float Sigma1 = 0.2;
        float Sigma2 = 0.2;
        float X;
        float Y;
        
        // F = mvnpdf([X1(:) X2(:)],mean,Sigma);
        for(int i=0;i<splits;i++){
            for(int j=0;j<splits;j++){
                X = i*dx - mean1;
                Y = j*dx - mean2;
                grid[i][j] = (1/(2 * M_PI * sqrt(Sigma1 * Sigma2))) * exp(-0.5 *((X*X)/(Sigma1 * Sigma1) + (Y * Y)/(Sigma2 * Sigma2)));
                grid_n[i][j]=grid[i][j];
            }
        }
    }
    void print_field (string outpath) override{
        ofstream outNutrientField(outpath);
        for(int i=1;i<splits-1;i++){
            for(int j=1;j<splits-2;j++){
                outNutrientField<<grid[i][j]<<",";
            }
            outNutrientField<<grid[i][splits-2]<<endl;
        }
    }
};

#endif
