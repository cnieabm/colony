//
//  main.cpp
//  Colony
//
//  Created by Michalis Michaelides on 27/01/2015.
//  Copyright (c) 2015 UCL. All rights reserved.
//

#include <iostream>
#include <fstream>
#include "Simulation.h"
#include "Settings.h"

using namespace std;

int main(int argc, char * argv[]) {
    string setpath = "/Users/michalis/Documents/UCL/ABM/Colony/";
//    cout<<getcwd()<<endl;
    Settings setting(setpath+"settings.txt");
    setting.OutSettings();
    
    //Define Simulation
    Simulation Test(&setting);
    Test.Run();
    return 0;
}
