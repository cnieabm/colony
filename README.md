# README #

### Summary ###

* Colony is a developing Agent-based simulation for bacteria-like colonies.
* In alpha.

### Set up ###
Compilers:

* A C/C++ compiler (tested on Mac: clang)

Libraries:

* Eigen (version 3) http://eigen.tuxfamily.org
* SDL2 (version 2) http://libsdl.org/index.php
* Make sure to include this in the project's header / library search paths to be included in build. (Xcode settings are in repo.)

### Contact ###
ucabmm3@ucl.ac.uk